package com.infermc.phoenix.christmas;

import com.google.gson.Gson;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.MinecraftServer;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_10_R1.PlayerInteractManager;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

public class SantaHats extends JavaPlugin implements Listener {
    HashMap<UUID,Boolean> enableStatus = new HashMap<UUID, Boolean>();

    // API Key that allows the plugin to work
    String key = "";
    String event = "";
    String apiURL = "";
    boolean debug = false;

    public void onEnable() {
        saveDefaultConfig();

        // Load in some stuff for the API
        key = getConfig().getString("key","");
        event = getConfig().getString("event","none");
        apiURL = getConfig().getString("api","");
        debug = getConfig().getBoolean("debug",false);

        if (apiURL.equalsIgnoreCase("")) {
            getLogger().warning("Unable to proceed! Please set the API url in the config!");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        getServer().getPluginManager().registerEvents(this,this);
        loadStatus();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String cmd = command.getName();
        Player p = null;
        if (sender instanceof Player) p = (Player) sender;

        if (cmd.equalsIgnoreCase("togglehat") && sender.hasPermission("santahat.toggle")) {
            if (p != null) {
                Boolean current = true;
                if (enableStatus.containsKey(p.getUniqueId())) {
                    current = enableStatus.get(p.getUniqueId());
                }
                enableStatus.put(p.getUniqueId(),!current);
                getLogger().info(p.getName()+" has turned their hat "+(!current==true ? "on" : "off"));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&',"&aYour hat has been turned "+(!current==true ? "on" : "off")+", please reconnect!"));
                saveStatus();
            }
        }

        return super.onCommand(sender, command, label, args);
    }

    public void saveStatus() {
        File statusFile = new File(getDataFolder()+"/status.yml");
        YamlConfiguration statusCFG = new YamlConfiguration();

        for (Map.Entry<UUID,Boolean> cur : enableStatus.entrySet()) {
            // Dont care about enabled hats (They're on by default)
            if (cur.getValue()) continue;
            statusCFG.set(cur.getKey().toString(),cur.getValue());
        }
        try {
            statusCFG.save(statusFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void loadStatus() {
        File statusFile = new File(getDataFolder()+"/status.yml");
        if (!statusFile.exists()) return;
        YamlConfiguration statusCFG = new YamlConfiguration();
        try {
            statusCFG.load(statusFile);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return;
        }
        enableStatus.clear();
        for (String sec : statusCFG.getRoot().getKeys(false)) {
            UUID uid = UUID.fromString(sec);
            Boolean status = statusCFG.getRoot().getBoolean(sec,true);
            // Dont care about enabled hats (They're on by default)
            if (status) continue;
            enableStatus.put(uid,status);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent ev) {
        final Player p = ev.getPlayer();
        applyHat(p);
    }

    public void applyHat(Player p) {

        Boolean status = true;

        if (enableStatus.containsKey(p.getUniqueId())) {
            status = enableStatus.get(p.getUniqueId());
        }

        if (status) {
            boolean res = changeSkin(p, p.getUniqueId().toString());
            if (res) {
                if (debug) getLogger().info("Applying hat for "+p.getName());
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aHey " + p.getDisplayName() + ",&r&a Enjoy your hat :)"));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDon't want it on? Toggle it off with /togglehat then reconnect!"));
            } else {
                if (debug) getLogger().warning("Error applying hat for "+p.getName());
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cWhoops we had an issue applying your hat, our skin server may be busy :("));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can try waiting a few minutes and then reconnecting."));
            }
        } else {
            if (debug) getLogger().info("Not applying hat on "+p.getName()+", they have it turned off.");
            p.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cHey "+p.getDisplayName()+"&r&c, Your hat is turned off."));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cYou can turn it back on again anytime, Just type /togglehat then reconnect!"));
        }
    }

    public boolean changeSkin(Player p, String uuid) {
        URL api;
        try {
            api = new URL(apiURL+"?uuid="+uuid+"&event="+event+"&key="+key);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        }
        String inputLine = "";
        try {
            inputLine = IOUtils.toString(api, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        if (inputLine == "") {
            return false;
        }
        Gson gson = new Gson();
        JSONObject json = gson.fromJson(inputLine, JSONObject.class);

        if(json==null) {
            if (debug) {
                getLogger().warning("Error obtaining skin data for "+p.getName()+", Invalid json?");
                getLogger().info(inputLine);
            }
            return false;
        }

        String value = (String) json.get("texture");
        String signature = (String) json.get("signature");

        if (value == null) {
            if (debug) {
                getLogger().warning("Error obtaining skin data for "+p.getName()+", Error getting 'texture' property from API!");
                getLogger().info(inputLine);
            }
            return false;
        }
        if (signature == null) {
            if (debug) {
                getLogger().warning("Error obtaining skin data for "+p.getName()+", Error getting 'signature' property from API!");
                getLogger().info(inputLine);
            }
            return false;
        }
        return true;
    }

    public void updateSkin(final Player p, String texture, String signature) {

        final CraftPlayer cp = (CraftPlayer) p;

        // Get their profile
        GameProfile profile = cp.getProfile();

        // Change texture
        profile.getProperties().removeAll("textures");
        profile.getProperties().put("textures", new Property("textures", texture, signature));

        final List<Player> canSee = new ArrayList<Player>();
        for (Player player1 : Bukkit.getOnlinePlayers()) {
            if (player1.canSee(p)) {
                canSee.add(player1);
                player1.hidePlayer(p);
            }
        }
        Bukkit.getScheduler().runTaskLater(this, new Runnable() {
            @Override
            public void run() {
                for (Player player1 : canSee) {
                    player1.showPlayer(p);
                }
            }
        },10L);
    }
}
